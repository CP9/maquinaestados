﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Accion : MonoBehaviour
{
    public bool vivo = true;
    Animator animator; 
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis("Golpe") != 0 )
        {
            //GOLPEAR
            animator.SetBool("Golpeando",true);
        }
        if (Input.GetAxis("Muerte") != 0 )
        {
            //Morir
            animator.SetBool("Vivo",false);
        }
        if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
        {
            animator.SetBool("Corriendo", true );

        }
        else
        {
                animator.SetBool("Corriendo", false );
        }


    }
}
